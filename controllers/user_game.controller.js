const express = require('express');
const { userGames, userGameBiodata } = require('../models');

const getUserAll = async (req, res) => {
  const userGame = await userGames.findAll({});
  res.status(200).send({
    status: 200,
    message: 'berhasil',
    data: userGame,
  });
};

const getUserBiodataAll = async (req, res) => {
  const usergamebiodata = await userGameBiodata.findAll({});
  res.status(200).send({
    status: 200,
    message: 'berhasil',
    data: usergamebiodata,
  });
};

const getUserById = async (req, res) => {
  const userGame = await userGames.findAll({
    where: {
      id: req.params.id,
    },
  });
  res.status(200).send({
    status: 200,
    message: 'berhasil',
    data: userGame,
  });
};

const createUser = async (req, res) => {
  const { username, password } = req.body;
  const user = await userGames.create({
    username,
    password,
  });
  const { email, nohp } = req.body;
  const userBiodata = await userGameBiodata.create({
    email,
    nohp,
    userId: user.id,
  });
  res.status(200).send({
    status: 200,
    message: 'berhasil tambah data',
    data: userBiodata,
    user,
  });
};
const updateUser = async (req, res) => {
  const updateuser = await userGames.update(req.body, {
    where: {
      id: req.params.id,
    },
  });
  res.status(200).send({
    status: 200,
    message: 'berhasil update data',
    data: updateuser,
  });
};
const deleteUserById = async (req, res) => {
  const deleteuserid = await userGames.destroy({
    where: {
      id: req.params.id,
    },
  });
  res.status(200).send({
    status: 200,
    message: 'berhasil delete data',
  });
};

module.exports = {
  getUserAll,
  getUserBiodataAll,
  getUserById,
  deleteUserById,
  updateUser,
  createUser,
};
