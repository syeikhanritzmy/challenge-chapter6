'use strict';
const { Model } = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class userGameBiodata extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {}
  }
  userGameBiodata.init(
    {
      email: DataTypes.STRING,
      nohp: DataTypes.STRING,
      userId: {
        type: DataTypes.INTEGER,
        onDelete: 'CASCADE',
        references: {
          model: 'userGames',
          allowNull: true,
          key: 'id',
        },
      },
    },
    {
      sequelize,
      modelName: 'userGameBiodata',
    }
  );
  return userGameBiodata;
};
