const PointGame = {
  scissor: {
    rock: -1,
    paper: 1,
    scissor: 0,
  },
  rock: {
    scissor: 1,
    paper: -1,
    rock: 0,
  },
  paper: {
    scissor: -1,
    rock: 1,
    paper: 0,
  },
};

const choiceText = {
  scissor: 'scissor',
  paper: 'paper',
  rock: 'rock',
};
const options = ['scissor', 'paper', 'rock'];

class Com {
  getChoice() {
    return options[Math.floor(Math.random() * options.length)];
  }
}

class Player {
  theChoice = '';
  constructor(choice) {
    this.theChoice = choice;
  }
  getChoice() {
    return this.theChoice;
  }
}

class PlayerOptionCapturer {
  choiceCaptured = () => {};
  constructor(choiceCaptured) {
    this.choiceCaptured = choiceCaptured;
    this.init();
  }
  init() {
    this.listenUserChoice();
  }
  listenUserChoice() {
    const options = document.querySelectorAll('.playerchoice');
    const handleOptionClick = (ev) => {
      ev.target.classList.add('hand-selected');
      console.log('user habis click');
      this.choiceCaptured(ev.target.id);
      options.forEach((option) => {
        option.removeEventListener('click', handleOptionClick);
      });
    };
    options.forEach((option) => {
      option.addEventListener('click', handleOptionClick);
    });
  }
}

class ComView {
  theComChoice = '';
  constructor(comChoice) {
    this.theComChoice = comChoice;
  }

  highlightChoice() {
    const element = document.getElementById(
      `imageCom-${this.theComChoice.toLocaleLowerCase()}`
    );
    element.classList.add('hand-selected');
  }
}

function setWinnerText(text) {
  const el = document.getElementById('winner');
  el.classList.remove('text-vs');
  el.classList.add('text-winner');
  el.innerHTML = text;
}

function startTheGame() {
  new PlayerOptionCapturer(function (playerChoice) {
    const player = new Player(playerChoice);
    const com = new Com();
    const verdict = new Verdict(player, com);
    const result = verdict.getVerdict();

    setWinnerText(result.winner);
  });
}

startTheGame();
const resetBtn = document.querySelector('.refresh');
resetBtn.addEventListener('click', () => {
  const list = document.querySelectorAll('div');
  const ell = document.querySelector('#winner');
  list.forEach((el) => {
    el.classList.remove('hand-selected');
  });
  ell.classList.add('text-vs');
  ell.classList.remove('text-winner');
  ell.innerHTML = 'VS';
  startTheGame();
});

class Verdict {
  constructor(player, com) {
    this.player = player;
    this.com = com;
  }
  getVerdict() {
    const playerChoice = this.player.getChoice();
    const comChoice = this.com.getChoice();
    const playerScore = PointGame[playerChoice][comChoice];
    const comScore = PointGame[comChoice][playerChoice];

    const result = {
      playerChoice: choiceText[playerChoice],
      comChoice: choiceText[comChoice],
      winner: 'DRAW',
    };
    const comView = new ComView(comChoice);
    comView.highlightChoice();
    if (playerScore > comScore) result.winner = 'Player WIN';
    if (playerScore < comScore) result.winner = 'COMPUTER WIN';
    return result;
  }
}
