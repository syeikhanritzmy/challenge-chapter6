const express = require('express');
const { Get, Post } = require('../controllers/signIn');
const Router = express.Router();

Router.route('/signIn').get(Get).post(Post);

module.exports = Router;
