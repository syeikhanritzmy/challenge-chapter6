const express = require('express');
const router = express.Router();

const Usergame = require('./usergame.routes');

router.use('/dashboard', Usergame);

module.exports = router;
