const express = require('express');
const router = express.Router();
const Usergame = require('../controllers/user_game.controller');

router.get('/', Usergame.getUserAll);
router.get('/:id', Usergame.getUserById);
router.get('/biodata', Usergame.getUserBiodataAll);
router.put('/:id', Usergame.updateUser);
router.delete('/:id', Usergame.deleteUserById);
router.post('/', Usergame.createUser);

module.exports = router;
